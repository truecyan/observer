﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PopupCtrl : MonoBehaviour
{
    private static readonly Vector3 PoolingPos = new Vector3(0, -1080, 0);
    public Vector3 ClickPos;
    public Vector3 InitPos;
    public Text TitleText;
    public string PopupTitle;
    public GameObject Label;
    public bool IsEvidence;
    public bool IsTemp;
    public GameObject CloseMark;
    public GameObject AddMark;
    public bool OnArchive;
    public Image TopBar;
    public GameObject FocusMask;
    public Image TopBarColor;
    public Color FocusedColor;
    public Color OriginalColor;

    [SerializeField]private bool _isDragging;
    private RectTransform _tf;
    private float _topBarOffset = 50;

    // Start is called before the first frame update
    void Awake()
    {
        _tf = GetComponent<RectTransform>();
    }

    void Start()
    {

        TitleText.text = PopupTitle;
        InitPos = _tf.position;
        Label.transform.SetAsLastSibling();
        if (IsTemp)
        {
            CloseMark.SetActive(false);
        }
        FocusMask.transform.SetAsLastSibling();
    }

    // Update is called once per frame
    void Update()
    {
        if ((gameObject.transform.parent.childCount - 1 != gameObject.transform.GetSiblingIndex() || GameManager.Instance.CurrEvidence)
            && GameManager.Instance.CurrEvidence != gameObject)
        {
            FocusMask.SetActive(true);
            TopBarColor.color = OriginalColor;
        }
        else
        {
            FocusMask.SetActive(false);
            TopBarColor.color = FocusedColor;
        }


        if (_isDragging)
        {
            _tf.position = InitPos + (Input.mousePosition - ClickPos);
        }

        if (Input.GetMouseButtonUp(0))
        {
            _isDragging = false;
            if (IsEvidence)
            {
                transform.SetParent(GameManager.Instance.FieldCanvas.transform);
                GameManager.Instance.CurrEvidence = null;
                GameManager.Instance.ArchiveMask.SetActive(false);
                if (OnArchive && !GameManager.Instance.EvidenceFinalList.Contains(PopupTitle))
                {
                    GameManager.Instance.EvidenceFinalList.Add(PopupTitle);
                    if (GameManager.Instance.EvidenceDict.TryGetValue(PopupTitle, out var obj))
                    {
                        var newObj = Instantiate(obj);
                        var prop = newObj.GetComponent<EvidenceProperty>();
                        GameManager.Instance.WeightUpdate(prop.Weight);
                        Destroy(newObj);
                    }
                    GameManager.Instance.CostDeduct(3);
                    GameManager.Instance.EvidenceFinalListObj.GetComponent<EvidenceFinalList>().Refresh();
                    Close();
                }

                if (OnArchive)
                {
                    _tf.position = InitPos;
                }
                AddMode(false);

                GetComponent<Image>().raycastTarget = true;
                TopBar.GetComponent<Image>().raycastTarget = true;
            }
            if(IsTemp) Destroy(gameObject);
        }
    }

    public void StartDrag()
    {
        _isDragging = true;
        if (IsEvidence)
        {
            GetComponent<Image>().raycastTarget = false;
            TopBar.GetComponent<Image>().raycastTarget = false;
            transform.SetParent(GameManager.Instance.MenuCanvas.transform);
            GameManager.Instance.CurrEvidence = gameObject;
            GameManager.Instance.ArchiveMask.SetActive(true);
        }
        gameObject.transform.SetAsLastSibling();
        ClickPos = Input.mousePosition;
        InitPos = _tf.position;
    }

    public void Focus()
    {
        gameObject.transform.SetAsLastSibling();
    }
    public void Close()
    {
        if (IsEvidence)
        {
            GameManager.Instance.OpenedList.Remove(PopupTitle);
            Destroy(gameObject);
        }
        InitPos = _tf.position;
        _tf.position = PoolingPos;

    }
    public void Open()
    {
        _tf.position = InitPos;
        gameObject.transform.SetAsLastSibling();
    }

    public void SetSize(float x, float y)
    {
        _tf.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,x);
        _tf.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,y);
    }
    public void SetInitial(string type)
    {
        switch (type)
        {
            case "Phone":
                SetSize(1000, 500);
                Instantiate(GameManager.Instance.PhoneMenu, transform);
                break;
            case "SNS":
                Instantiate(GameManager.Instance.SNSMenu, transform);
                break;
            case "PC":
                SetSize(600, 800);
                Instantiate(GameManager.Instance.PCMenu, transform);
                break;
            case "Profile":
                SetSize(600, 800);
                Instantiate(GameManager.Instance.ProfileMenu, transform);
                break;
            case "CCTV":
                SetSize(800, 600);
                Instantiate(GameManager.Instance.CCTVMenu, transform);
                break;
            case "GPS":
                SetSize(600, 800);
                Instantiate(GameManager.Instance.GPSMenu, transform);
                break;
            default:
                if (GameManager.Instance.EvidenceDict.TryGetValue(type, out var obj))
                {
                    var newObj = Instantiate(obj, transform);
                    var prop = newObj.GetComponent<EvidenceProperty>();
                    var size = prop.GetProperSize();
                    var rect = newObj.GetComponent<RectTransform>();
                    _tf.sizeDelta = new Vector2(size.x,size.y + _topBarOffset);
                    rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, -_topBarOffset);
                    prop.Adjust();
                    IsEvidence = true;
                }
                break;
        }
    }

    public void AddMode(bool set)
    {
        OnArchive = set;
        AddMark.SetActive(set);
    }
}
