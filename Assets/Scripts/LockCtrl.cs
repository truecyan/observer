﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockCtrl : MonoBehaviour
{
    public string Passcode = "0451";
    public string _input = "";
    public Color OriginalColor;
    public Color WrongColor;
    public GameObject[] PasscodeMasks = new GameObject[4];
    public PhoneMenu Phone;
    public PCMenu PC;
    public bool IsPC;
    public GameObject Back;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (IsPC)
        {
            if (PC.transform.parent.GetSiblingIndex() == PC.transform.parent.parent.childCount - 1)
            {
                if(Input.GetKeyDown(KeyCode.Keypad0)||Input.GetKeyDown(KeyCode.Alpha0))
                {
                    InputCode("0");
                }
                if(Input.GetKeyDown(KeyCode.Keypad1)||Input.GetKeyDown(KeyCode.Alpha1))
                {
                    InputCode("1");
                }
                if(Input.GetKeyDown(KeyCode.Keypad2)||Input.GetKeyDown(KeyCode.Alpha2))
                {
                    InputCode("2");
                }
                if(Input.GetKeyDown(KeyCode.Keypad3)||Input.GetKeyDown(KeyCode.Alpha3))
                {
                    InputCode("3");
                }
                if(Input.GetKeyDown(KeyCode.Keypad4)||Input.GetKeyDown(KeyCode.Alpha4))
                {
                    InputCode("4");
                }
                if(Input.GetKeyDown(KeyCode.Keypad5)||Input.GetKeyDown(KeyCode.Alpha5))
                {
                    InputCode("5");
                }
                if(Input.GetKeyDown(KeyCode.Keypad6)||Input.GetKeyDown(KeyCode.Alpha6))
                {
                    InputCode("6");
                }
                if(Input.GetKeyDown(KeyCode.Keypad7)||Input.GetKeyDown(KeyCode.Alpha7))
                {
                    InputCode("7");
                }
                if(Input.GetKeyDown(KeyCode.Keypad8)||Input.GetKeyDown(KeyCode.Alpha8))
                {
                    InputCode("8");
                }
                if(Input.GetKeyDown(KeyCode.Keypad9)||Input.GetKeyDown(KeyCode.Alpha9))
                {
                    InputCode("9");
                }
                if(Input.GetKeyDown(KeyCode.Backspace))
                {
                    RemoveCode();
                }
            }
        }
    }

    public void InputCode(string s)
    {
        _input += s;
        if(_input.Length <= Passcode.Length)
            PasscodeMasks[_input.Length-1].SetActive(true);
        if (_input.Length >= Passcode.Length)
        {
            StartCoroutine(CheckCode());
        }
    }

    public void RemoveCode()
    {
        if (_input.Length <= 0) return;
        PasscodeMasks[_input.Length-1].SetActive(false);
        _input = _input.Substring(0,_input.Length-1);
    }

    private IEnumerator CheckCode()
    {
        yield return new WaitForSeconds(0.2f);
        if (Passcode == _input)
        {
            if (IsPC)
            {
                PC.ClickElement("Back");
                Back.SetActive(true);
            }
            else Phone.ClickElement("Back");
        }
        else
        {
            foreach (var pass in PasscodeMasks)
            {
                if (IsPC) pass.GetComponent<Text>().color = WrongColor;
                else pass.GetComponent<Image>().color = WrongColor;
            }
            yield return new WaitForSeconds(0.5f);
            foreach (var pass in PasscodeMasks)
            {
                if (IsPC) pass.GetComponent<Text>().color = OriginalColor;
                else pass.GetComponent<Image>().color = OriginalColor;
                pass.SetActive(false);
            }
            _input = "";
        }
    }
}
