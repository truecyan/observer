﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneMenu : MonoBehaviour
{
    public GameObject ActiveScreen;
    public GameObject[] ScreenList;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ClickElement(string n)
    {
        //GameManager.Instance.focused = gameObject;
        switch (n)
        {
            case "Back":
                Refresh(0);
                break;
            case "Call":
                if (!GameManager.Instance.SearchedList.Contains(n))
                {
                    GameManager.Instance.CostDeduct(3);
                    GameManager.Instance.SearchedList.Add(n);
                }
                Refresh(1);
                break;
            case "Text":
                Refresh(2);
                break;
            case "Calendar":
                Refresh(3);
                break;
            case "Amy":
                if (!GameManager.Instance.SearchedList.Contains(n))
                {
                    GameManager.Instance.CostDeduct(1);
                    GameManager.Instance.SearchedList.Add(n);
                }
                Refresh(4);
                break;
            case "James":
                if (!GameManager.Instance.SearchedList.Contains(n))
                {
                    GameManager.Instance.CostDeduct(1);
                    GameManager.Instance.SearchedList.Add(n);
                }
                Refresh(5);
                break;
            case "Prof":
                if (!GameManager.Instance.SearchedList.Contains(n))
                {
                    GameManager.Instance.CostDeduct(1);
                    GameManager.Instance.SearchedList.Add(n);
                }
                Refresh(6);
                break;
            case "Spam":
                if (!GameManager.Instance.SearchedList.Contains(n))
                {
                    GameManager.Instance.CostDeduct(1);
                    GameManager.Instance.SearchedList.Add(n);
                }
                Refresh(7);
                break;
            default:
                break;
        }
    }

    public void Refresh(int n)
    {
        ActiveScreen = ScreenList[n];
        foreach (var screen in ScreenList)
        {
            screen.SetActive(ActiveScreen == screen);
        }
    }
}
