﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceListCtrl : MonoBehaviour
{
    public RectTransform content;
    public string[] EvidenceList;

    // Start is called before the first frame update
    void Start()
    {
        float totalLength = 0;
        foreach (var evidence in EvidenceList)
        {
            if (!GameManager.Instance.EvidenceDict.TryGetValue(evidence, out var obj)) continue;
            var newObj = Instantiate(obj, content.transform);
            var prop = newObj.GetComponent<EvidenceProperty>();
            var rect = newObj.GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, -totalLength);
            prop.Adjust();
            totalLength += prop.GetHeight();
        }
        content.sizeDelta = new Vector2(content.sizeDelta.x,totalLength);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
