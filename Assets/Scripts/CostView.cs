﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CostView : MonoBehaviour
{
    private RectTransform _tf;
    public Text CostText;
    public bool rightOrigin;
    private float _offset = -150;

    // Start is called before the first frame update
    void Awake()
    {
        _tf = gameObject.GetComponent<RectTransform>();
    }

    void Start()
    {
        _offset = -_tf.rect.width;
    }

    // Update is called once per frame
    void Update()
    {
        Initialize();
    }

    public void Initialize()
    {
        if (rightOrigin)
        {
            _tf.position = Input.mousePosition;
            _tf.anchoredPosition += new Vector2(_offset, 0);
        }
        else
        {
            _tf.position = Input.mousePosition;
        }
    }
    public void SetText(int cost)
    {
        CostText.text = "Cost -" + cost;
    }

}
