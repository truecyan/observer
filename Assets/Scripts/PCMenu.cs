﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCMenu : MonoBehaviour
{
    public GameObject ActiveFolder;
    private int _currNum = 7;
    public GameObject[] FolderList;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ClickElement(string n)
    {
        //GameManager.Instance.focused = gameObject;
        switch (n)
        {
            case "Back":
                Refresh(0);
                break;
            case "Log":
                Refresh(1);
                break;
            case "Music":
                if (!GameManager.Instance.SearchedList.Contains(n))
                {
                    GameManager.Instance.CostDeduct(1);
                    GameManager.Instance.SearchedList.Add(n);
                }
                Refresh(2);
                break;
            case "Papers":
                Refresh(3);
                break;
            case "Pictures":
                Refresh(4);
                break;
            case "PurchaseHistory":
                Refresh(5);
                break;
            case "SearchHistory":
                Refresh(6);
                break;
            default:
                break;
        }
    }

    public void Refresh(int n)
    {
        if (n != 0)
        {
            ActiveFolder = FolderList[n];
            _currNum = n;
        }
        else
        {
            switch (_currNum)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 7:
                    ActiveFolder = FolderList[0];
                    _currNum = 0;
                    break;
                case 5:
                case 6:
                    ActiveFolder = FolderList[1];
                    _currNum = 1;
                    break;
                default:
                    break;
            }
        }
        foreach (var folder in FolderList)
        {
            folder.SetActive(ActiveFolder == folder);
        }
    }
}
