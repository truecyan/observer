﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragCtrl : MonoBehaviour
{
    public bool OnArchive;
    public GameObject AddSign;
    public string EvidenceName;
    public Text EvidenceText;

    private RectTransform _tf;
    // Start is called before the first frame update
    void Start()
    {
        _tf = gameObject.GetComponent<RectTransform>();
        EvidenceText.text = EvidenceName;
    }

    // Update is called once per frame
    void Update()
    {
        _tf.position = Input.mousePosition;
        if (Input.GetMouseButtonUp(0))
        {
            if (OnArchive && !GameManager.Instance.EvidenceFinalList.Contains(EvidenceName))
            {
                GameManager.Instance.EvidenceFinalList.Add(EvidenceName);
                if (GameManager.Instance.EvidenceDict.TryGetValue(EvidenceName, out var obj))
                {
                    var newObj = Instantiate(obj);
                    var prop = newObj.GetComponent<EvidenceProperty>();
                    GameManager.Instance.WeightUpdate(prop.Weight);
                    Destroy(newObj);
                }
                GameManager.Instance.CostDeduct(3);
                GameManager.Instance.EvidenceFinalListObj.GetComponent<EvidenceFinalList>().Refresh();
            }
            GameManager.Instance.ArchiveMask.SetActive(false);
            GameManager.Instance.CurrDrag = null;
            Destroy(gameObject);
        }
    }

    public void AddMode(bool set)
    {
        OnArchive = set;
        AddSign.SetActive(set);
    }
}
