﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ArchiveCtrl : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (GameManager.Instance.CurrDrag)
        {
            var ctrl = GameManager.Instance.CurrDrag.GetComponent<DragCtrl>();
            ctrl.AddMode(true);
        }

        if (GameManager.Instance.CurrEvidence)
        {
            var ctrl = GameManager.Instance.CurrEvidence.GetComponent<PopupCtrl>();
            ctrl.AddMode(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (GameManager.Instance.CurrDrag)
        {
            var ctrl = GameManager.Instance.CurrDrag.GetComponent<DragCtrl>();
            ctrl.AddMode(false);
        }
        if (GameManager.Instance.CurrEvidence)
        {
            var ctrl = GameManager.Instance.CurrEvidence.GetComponent<PopupCtrl>();
            ctrl.AddMode(false);
        }
    }
}
