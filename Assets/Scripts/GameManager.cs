﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
                if (_instance == null)
                {
                    var Object = Instantiate(Resources.Load<GameObject>("_GameManager/GameManager"));
                    _instance = Object.GetComponent<GameManager>();
                }
            }

            return _instance;
        }
    }

    public enum Type
    {
        Calendar,
        Call,
        CCTV,
        GPS,
        Music,
        Papers,
        Pictures,
        Profile,
        Log,
        SNS,
        Text,
        Others
    }

    public Sprite[] TypeIcons;

    public GameObject focused;
    public GameObject PhoneMenu;
    public GameObject SNSMenu;
    public GameObject PCMenu;
    public GameObject CCTVMenu;
    public GameObject GPSMenu;
    public GameObject ProfileMenu;
    public GameObject CriminalCaseLogMenu;
    public GameObject FieldCanvas;
    public GameObject MenuCanvas;
    public GameObject NameElement;
    public GameObject DragObject;

    public GameObject ArchiveMask;
    public GameObject CurrDrag;
    public GameObject CurrEvidence;
    public GameObject SubmitButton;

    public GameObject Readme;
    public GameObject[] Endings = new GameObject[3];
    public GameObject Replay;

    public int InitCost = 50;
    public int CostLeft = 50;
    public Text CostText;

    public GameObject CostView;

    public int TotalWeight = 0;
    public int WeightGoal = 100;
    public int WeightGoalPlus = 150;
    public Image WeightBar;
    public Text WeightText;

    public Dictionary<string,Dictionary<string,GameObject>> PopupDict = new Dictionary<string,Dictionary<string,GameObject>>();
    public Dictionary<string,GameObject> EvidenceDict = new Dictionary<string,GameObject>();
    public GameObject CriminalCaseLogObj;
    public List<string> OpenedList = new List<string>();
    public List<string> SearchedList = new List<string>();
    public List<string> EvidenceFinalList = new List<string>();
    public Dictionary<string,GameObject> EvidenceFinalDict = new Dictionary<string, GameObject>();
    public GameObject EvidenceFinalListObj;
    public GameObject EvidenceFinalElem;

    void Awake ()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);

        //DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        CostLeft = InitCost;
        CostText.text = CostLeft.ToString();

        var dictA = new Dictionary<string, GameObject>
        {
            {"Profile", null},
            {"SNS", null},
            {"CCTV", null},
            {"GPS", null},
            {"Phone", null},
            {"PC", null}
        };
        var dictB = new Dictionary<string, GameObject>
        {
            {"Profile", null},
            {"SNS", null},
            {"CCTV", null},
            {"GPS", null},
            {"Phone", null},
            {"PC", null}
        };
        var dictC = new Dictionary<string, GameObject>
        {
            {"Profile", null},
            {"SNS", null},
            {"CCTV", null},
            {"GPS", null},
            {"Phone", null},
            {"PC", null}
        };

        PopupDict.Add("A",dictA);
        PopupDict.Add("B",dictB);
        PopupDict.Add("C",dictC);

        foreach (var obj in Resources.LoadAll<GameObject>("Prefab/Evidence"))
        {
            EvidenceDict.Add(obj.name, obj);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetMouseButtonUp(0))
        {
            focused = null;
        }
    }

    public void CostDeduct(int cost)
    {
        CostLeft -= cost;
        CostText.text = CostLeft.ToString();
        if(CostLeft < 0) ShowEnding(0);
    }

    public void Submit()
    {
        if(TotalWeight >= WeightGoalPlus) ShowEnding(2);
        else if(TotalWeight >= WeightGoal) ShowEnding(1);
    }

    public void WeightUpdate(int weight)
    {
        TotalWeight += weight;
        WeightBar.fillAmount = TotalWeight / (float)WeightGoal;
        WeightText.text = Mathf.RoundToInt(TotalWeight / (float)WeightGoal * 100) + "%";

        SubmitButton.SetActive(TotalWeight >= WeightGoal);

    }

    public Sprite GetTypeIcon(Type t)
    {
        switch (t)
        {
            case Type.Calendar:
                return TypeIcons[0];
            case Type.Call:
                return TypeIcons[1];
            case Type.CCTV:
                return TypeIcons[2];
            case Type.GPS:
                return TypeIcons[3];
            case Type.Music:
                return TypeIcons[4];
            case Type.Papers:
                return TypeIcons[5];
            case Type.Pictures:
                return TypeIcons[6];
            case Type.Profile:
                return TypeIcons[7];
            case Type.Log:
                return TypeIcons[8];
            case Type.SNS:
                return TypeIcons[9];
            case Type.Text:
                return TypeIcons[10];
            case Type.Others:
                return TypeIcons[11];
            default:
                throw new ArgumentOutOfRangeException(nameof(t), t, null);
        }
    }

    public void ShowEnding(int n)
    {
        Endings[n].SetActive(true);
        Replay.SetActive(true);
    }

    public void Reload()
    {
        TutorialSkipper.TutorialSkip = true;
        Initiate.Fade(SceneManager.GetActiveScene().name,Color.black,1f);
    }
}
