﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawerCtrl : MonoBehaviour
{
    public static float DrawTime = 0.5f;
    public static float InitPos = -900f;
    public static float ToPos = 0f;
    public GameObject PopupTemplate;
    public string DrawerName;

    private bool _isFocused = false;
    private RectTransform _tf;
    private Transform _canvas;

    // Start is called before the first frame update
    void Start()
    {
        _tf = GetComponent<RectTransform>();
        _canvas = gameObject.transform.parent;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isFocused && GameManager.Instance.focused == gameObject)
        {
            _isFocused = true;
        }
        else if (_isFocused && GameManager.Instance.focused != gameObject)
        {
            _isFocused = false;
        }

        if (_isFocused)
        {
            _tf.anchoredPosition += new Vector2((ToPos-InitPos)/DrawTime*Time.deltaTime,0);
            if(_tf.anchoredPosition.x > ToPos)
                _tf.anchoredPosition = new Vector2(ToPos,_tf.anchoredPosition.y);
        }
        else
        {
            _tf.anchoredPosition += new Vector2((InitPos-ToPos)/DrawTime*Time.deltaTime,0);
            if (_tf.anchoredPosition.x < InitPos)
                _tf.anchoredPosition = new Vector2(InitPos,_tf.anchoredPosition.y);
        }
    }

    public void Click()
    {
        GameManager.Instance.focused = gameObject;
        gameObject.transform.SetAsLastSibling();
    }

    public void ClickElement(string n)
    {
        GameManager.Instance.focused = null;
        switch (n)
        {
            case "Criminal Case Log":
                PopUp(n);
                break;
            case "Profile":
                if (!GameManager.Instance.SearchedList.Contains(n))
                {
                    GameManager.Instance.CostDeduct(1);
                    GameManager.Instance.SearchedList.Add(n);
                }
                PopUp(n);
                break;
            case "SNS":
                if (!GameManager.Instance.SearchedList.Contains(n))
                {
                    GameManager.Instance.CostDeduct(3);
                    GameManager.Instance.SearchedList.Add(n);
                }
                PopUp(n);
                break;
            case "CCTV":
                PopUp(n);
                break;
            case "GPS":
                PopUp(n);
                break;
            case "Phone":
                PopUp(n);
                break;
            case "PC":
                PopUp(n);
                break;
            default:
                break;
        }
    }

    public void PopUp(string n)
    {
        if (n == "Criminal Case Log")
        {
            GameManager.Instance.CriminalCaseLogObj.GetComponent<PopupCtrl>().Open();
            return;
        }
        if (GameManager.Instance.PopupDict[DrawerName][n] == null)
        {
            var obj = Instantiate(PopupTemplate, _canvas);
            GameManager.Instance.PopupDict[DrawerName][n] = obj;
            var pop = obj.GetComponent<PopupCtrl>();
            pop.PopupTitle = n;
            pop.SetInitial(n);
        }
        else
        {
            GameManager.Instance.PopupDict[DrawerName][n].GetComponent<PopupCtrl>().Open();
        }
    }
}
