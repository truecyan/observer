﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceNameList : MonoBehaviour
{
    public RectTransform content;
    public string[] EvidenceList;

    // Start is called before the first frame update
    void Start()
    {
        float totalLength = 0;
        foreach (var evidence in EvidenceList)
        {
            var elem = Instantiate(GameManager.Instance.NameElement, content.transform);
            var rect = elem.GetComponent<RectTransform>();
            var ctrl = elem.GetComponent<NameElementCtrl>();
            ctrl.EvidenceName = evidence;
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, -totalLength);
            totalLength += ctrl.GetHeight();
        }
        content.sizeDelta = new Vector2(content.sizeDelta.x,totalLength);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
