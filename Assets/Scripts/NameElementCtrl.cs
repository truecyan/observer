﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameElementCtrl : MonoBehaviour
{
    public GameObject EvidencePopup;
    public Text Title;
    public string EvidenceName;
    public Color ReadColor;

    // Start is called before the first frame update
    void Start()
    {
        Title.text = EvidenceName;
        var imgs = GetComponentsInChildren<Image>();
        var img = imgs[imgs.Length-1];
        if (GameManager.Instance.EvidenceDict.TryGetValue(EvidenceName, out var obj))
        {
            var type = obj.GetComponent<EvidenceProperty>().EvidenceType;
            img.sprite = GameManager.Instance.GetTypeIcon(type);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Open()
    {
        if (GameManager.Instance.OpenedList.Contains(EvidenceName)) return;
        if (!GameManager.Instance.SearchedList.Contains(EvidenceName))
        {
            GameManager.Instance.CostDeduct(1);
            GameManager.Instance.SearchedList.Add(EvidenceName);
        }
        var obj = Instantiate(EvidencePopup,GameManager.Instance.FieldCanvas.transform);
        var pop = obj.GetComponent<PopupCtrl>();
        Title.color = ReadColor;
        pop.PopupTitle = EvidenceName;
        pop.SetInitial(EvidenceName);
        GameManager.Instance.OpenedList.Add(EvidenceName);
    }

    public float GetHeight()
    {
        return gameObject.GetComponent<RectTransform>().rect.height;
    }
}
