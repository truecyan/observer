﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceOpener : MonoBehaviour
{
    public GameObject EvidencePopup;
    public string EvidenceName;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Open()
    {
        if (GameManager.Instance.OpenedList.Contains(EvidenceName)) return;
        if (!GameManager.Instance.SearchedList.Contains(EvidenceName))
        {
            GameManager.Instance.CostDeduct(1);
            GameManager.Instance.SearchedList.Add(EvidenceName);
        }
        var obj = Instantiate(EvidencePopup,GameManager.Instance.FieldCanvas.transform);
        var pop = obj.GetComponent<PopupCtrl>();
        pop.PopupTitle = EvidenceName;
        pop.SetInitial(EvidenceName);
        GameManager.Instance.OpenedList.Add(EvidenceName);
    }
}
