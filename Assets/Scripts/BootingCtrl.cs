﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BootingCtrl : MonoBehaviour
{
    public string BootingText = "";
    public Text Output;

    public string[] BootingLines =
    {
        "***********************************************\n"
        +"** Virtual Investigation Tool v14.6.2\n"
        +"** Copyright (c) 2016 Hajesoft Corporation\n"
        +"***********************************************"
        ,""
        ,"C:\\Program Files\\Hajesoft Virtual Investigation> Observer.exe"
        ,""
        ,"National Cyber Research Institute (R) 2019 ver 0.2.5"
        ,"Copyright (C) NCRI. All rights and informations are classified."
        ,""
        ,"Checking Authority for investigation..."
        ,"Authority Checked [Case number: 3RD53592]"
        ,""
        ,"updating user data base (last update: 2019:05:01)"
        ,"Accessing files of all suspects..."
        ,"Building user Research Environment..."
        ,"Profile ------------ success"
        ,"GPS ---------------- success"
        ,"SNS ---------------- success"
        ,"CCTV --------------- success"
        ,"PC ----------------- locked"
        ,"Phone -------------- locked"
        ,"(file unlocked 4/6)"
        ,""
        ,"User defined method found: Observer ver 0.2.5"
        ,""
        ,"Initializing Observer in 3... 2... 1..."
        ,""
        ,"Starting Observer"
    };

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartBooting()
    {
        StartCoroutine(Iterate());
    }

    IEnumerator Iterate()
    {
        foreach (var s in BootingLines)
        {
            Debug.Log("line");
            BootingText += s + "\n";
            Output.text = BootingText;
            yield return new WaitForSeconds(0.2f);
        }
        yield return new WaitForSeconds(0.8f);
        transform.parent.gameObject.SetActive(false);
    }
}
