﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CostViewer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string EvidenceName;
    public int Cost;
    public bool IsAlways;

    private GameObject _costView;
    // Start is called before the first frame update
    void Start()
    {
        var elem = gameObject.GetComponent<NameElementCtrl>();
        if (elem)
        {
            EvidenceName = elem.EvidenceName;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!_costView && (IsAlways||!GameManager.Instance.SearchedList.Contains(EvidenceName))
                       && !GameManager.Instance.CurrDrag && !GameManager.Instance.CurrEvidence)
        {
            _costView = Instantiate(GameManager.Instance.CostView, GameManager.Instance.MenuCanvas.transform);
            var cv = _costView.GetComponent<CostView>();
            cv.SetText(Cost);
            cv.rightOrigin = IsAlways;
            cv.Initialize();
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_costView)
        {
            Destroy(_costView);
            _costView = null;
        }
    }

    private void OnDestroy()
    {
        if (_costView)
        {
            Destroy(_costView);
            _costView = null;
        }
    }
}
