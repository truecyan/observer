﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EvidenceFinal : MonoBehaviour,IPointerDownHandler
{
    public GameObject EvidencePopup;
    public Text Title;
    public Text WeightText;
    private int _weight;
    public string EvidenceName;

    // Start is called before the first frame update
    void Start()
    {
        Title.text = EvidenceName;
        var imgs = GetComponentsInChildren<Image>();
        var img = imgs[imgs.Length-1];
        if (GameManager.Instance.EvidenceDict.TryGetValue(EvidenceName, out var obj))
        {
            var newObj = Instantiate(obj);
            var prop = newObj.GetComponent<EvidenceProperty>();
            var type = prop.EvidenceType;
            WeightText.text = prop.Weight.ToString();
            _weight = prop.Weight;
            img.sprite = GameManager.Instance.GetTypeIcon(type);
            Destroy(newObj);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Open()
    {
        var obj = Instantiate(EvidencePopup,GameManager.Instance.FieldCanvas.transform);
        var pop = obj.GetComponent<PopupCtrl>();
        pop.PopupTitle = EvidenceName;
        pop.SetInitial(EvidenceName);
        pop.IsTemp = true;
        GameManager.Instance.OpenedList.Add(EvidenceName);
    }

    public void Remove()
    {
        GameManager.Instance.CostDeduct(5);
        GameManager.Instance.EvidenceFinalList.Remove(EvidenceName);
        GameManager.Instance.WeightUpdate(-_weight);
        GameManager.Instance.EvidenceFinalListObj.GetComponent<EvidenceFinalList>().Refresh();
        Destroy(gameObject);
    }

    public float GetHeight()
    {
        return gameObject.GetComponent<RectTransform>().rect.height;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Open();
    }
}
