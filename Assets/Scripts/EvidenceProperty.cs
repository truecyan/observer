﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EvidenceProperty : MonoBehaviour,IPointerDownHandler
{
    public int Weight;
    public int HOffset = 0;
    public GameManager.Type EvidenceType;

    private float _dragThreshold = 1.0f;
    private Vector3 _clickPos;
    private bool _isDragging;

    // Start is called before the first frame update
    void Awake()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            _isDragging = false;
        }
        if (_isDragging)
        {
            if ((_clickPos - Input.mousePosition).magnitude > _dragThreshold)
            {
                GameManager.Instance.ArchiveMask.SetActive(true);
                var obj = Instantiate(GameManager.Instance.DragObject,GameManager.Instance.MenuCanvas.transform);
                obj.transform.position = Input.mousePosition;
                GameManager.Instance.CurrDrag = obj;
                var ctrl = obj.GetComponent<DragCtrl>();
                ctrl.EvidenceName = GetName();
                ctrl.AddMode(false);
                _isDragging = false;
            }
        }
    }

    public float GetHeight()
    {
        return GetComponent<RectTransform>().rect.height;
    }

    public string GetName()
    {
        return gameObject.name.Replace("(Clone)","");
    }

    public void Adjust()
    {
        var pRect = GetComponentInParent<RectTransform>();
        var rect = GetComponent<RectTransform>();
        var imgs = GetComponentsInChildren<Image>();
        if (imgs.Length == 0)
        {
            return;
        }
        var img = imgs[imgs.Length-1];
        var txt = GetComponentInChildren<Text>();
        float offset = 0;
        if (txt != null)
        {
            offset = txt.GetComponent<RectTransform>().rect.height;
        }
        var w = img.sprite.rect.width;
        var h = img.sprite.rect.height - HOffset;
        rect.sizeDelta = new Vector2(0, pRect.rect.width / w * h + offset);
    }

    public Vector2 GetProperSize()
    {
        float properSize = 360000;
        var imgs = GetComponentsInChildren<Image>();
        if (imgs.Length == 0)
        {
            return new Vector2(800, 550);
        }
        var img = imgs[imgs.Length-1];
        var w = img.sprite.rect.width;
        var h = img.sprite.rect.height - HOffset;
        return Mathf.Sqrt(properSize / (w * h)) * new Vector2(w, h);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _clickPos = Input.mousePosition;
        _isDragging = true;
    }
}
