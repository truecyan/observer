﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceFinalList : MonoBehaviour
{
    public RectTransform content;
    private List<GameObject> _elements = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        Refresh();
    }

    public void Refresh()
    {

        float totalLength = 0;

        foreach (var elem in _elements)
        {
            Destroy(elem);
        }
        _elements.Clear();
        foreach (var evidence in GameManager.Instance.EvidenceFinalList)
        {
            var elem = Instantiate(GameManager.Instance.EvidenceFinalElem, content.transform);
            _elements.Add(elem);
            var rect = elem.GetComponent<RectTransform>();
            var ctrl = elem.GetComponent<EvidenceFinal>();
            ctrl.EvidenceName = evidence;
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, -totalLength);
            totalLength += ctrl.GetHeight();
        }
        content.sizeDelta = new Vector2(content.sizeDelta.x,totalLength);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
