﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialSkipper : MonoBehaviour
{
    public static bool TutorialSkip = false;

    private static TutorialSkipper _instance = null;
    public static TutorialSkipper Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<TutorialSkipper>();
            }

            return _instance;
        }
    }
    // Start is called before the first frame update
    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (TutorialSkip)
        {
            GameManager.Instance.Readme.SetActive(false);
        }
    }
}
